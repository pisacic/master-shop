import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyD9GJJ67nTyubeLwum_eCKm7SeD-tmb-is',
  authDomain: 'master-shop-7afe0.firebaseapp.com',
  projectId: 'master-shop-7afe0',
  storageBucket: 'master-shop-7afe0.appspot.com',
  messagingSenderId: '776432264419',
  appId: '1:776432264419:web:9d5b36160a9957b4f351d6',
  measurementId: 'G-9WRJ4RG10J',
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
