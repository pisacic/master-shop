import React from 'react';
import SignIn from '../../components/sign-in/sign-in.component';
import './sign-in-and-sign-out.styles.scss';

const SignInAndSignOut = () => {
  return (
    <div className='sign-in-and-sign-out'>
      <SignIn />
    </div>
  );
};

export default SignInAndSignOut;
